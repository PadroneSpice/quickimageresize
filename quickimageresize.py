# This Python file uses the following encoding: utf-8
import PIL
from PIL import Image
import glob
import sys
import os
import csv
import pathlib
from pathlib import Path
import PyQt5
from PyQt5.QtWidgets import QMessageBox, QWidget
from PyQt5.QtGui import QIntValidator
from PySide6 import QtCore
from PySide6.QtCore import Qt, QSize
from PySide6.QtWidgets import QApplication, QWidget, QLabel,\
    QPushButton, QHBoxLayout, QVBoxLayout, QCheckBox, QLineEdit, QFileDialog,\
    QListWidget, QListView, QGridLayout, QRadioButton


class QuickImageResize(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        inputFile = "rename_pairs.csv"
        self.PATH = os.getcwd()
        filepath_initial = Path(self.PATH + '\\' + inputFile)
        self.pairs = []
        #if filepath_initial.is_file():
        #    self.pairs = self.loadNames(inputFile)
        self.setWindowTitle("Image Resizer")

        self.message_percentage = QLabel("Resize Percentage:")
        self.message_percentage.alignment = Qt.AlignCenter
        # Text Entry for Resize Percentage
        self.linePercentage = QLineEdit()
        #self.linePercentage.setValidator(QIntValidator(0, 100, self.linePercentage))
        self.linePercentage.setText("100")

        # Radio Buttons
        self.message_filter = QLabel("Resampling Filter Select:")
        self.message_filter.alignment = Qt.AlignCenter
        self.radioButtonA = QRadioButton("Lanczos")
        self.radioButtonA.setChecked(True)
        self.radioButtonA.filter = 1
        self.currFilter = self.radioButtonA.filter
        self.radioButtonB = QRadioButton("Bicubic")
        self.radioButtonB.filter = 2

        # Image Location
        folderPathA = ""
        self.lineStart = QLineEdit()
        self.buttonStart = QPushButton("Choose Folder Of Images To Resize")

        # Saving Location
        folderPathB = ""
        self.lineDestination = QLineEdit()
        self.buttonDestination = QPushButton("Choose Destination Folder")

        # Checkbox for Rename
        self.checkRename = QCheckBox("Use rename_pairs.csv?")
        self.checkRename.setChecked(True)

        # Message
        self.message_resize = QLabel("Images to resize:")
        self.message_resize.alignment = Qt.AlignCenter

        # List of images to resize and rename
        #for item in pairs:
        self.listLabelA = QLabel("Initial File Names")
        self.listLabelB = QLabel("Destination File Names")
        self.listWidgetA = QListWidget()
        self.listWidgetB = QListWidget()
        self.populateLists()

        ### TODO: Implement this. It turns out that updating the list properly can be tricky because
        ### the signal is sent at every change (which is too often).
        # Make the litems in list B editable.
        #for index in range(self.listWidgetB.count()):
        #    item = self.listWidgetB.item(index)
        #    item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)

        # Buttons
        self.buttonResize = QPushButton("Resize By Percent")
        self.buttonExit = QPushButton("Exit")

        # Add the widgets
        self.mainLayout = QVBoxLayout(self)
        self.mainLayout.addWidget(self.message_percentage)
        self.mainLayout.addWidget(self.linePercentage)
        self.mainLayout.addWidget(self.message_filter)
        self.mainLayout.addWidget(self.radioButtonA)
        self.mainLayout.addWidget(self.radioButtonB)
        self.mainLayout.addWidget(self.buttonStart)
        self.mainLayout.addWidget(self.lineStart)
        self.mainLayout.addWidget(self.buttonDestination)
        self.mainLayout.addWidget(self.lineDestination)
        self.mainLayout.addWidget(self.checkRename)
        self.mainLayout.addWidget(self.message_resize)

        # Layout for two list columns
        self.listLayout = QGridLayout()
        self.mainLayout.addLayout(self.listLayout)
        self.listLayout.addWidget(self.listLabelA, 0, 0)
        self.listLayout.addWidget(self.listWidgetA, 1, 0)
        self.listLayout.addWidget(self.listLabelB, 0, 1)
        self.listLayout.addWidget(self.listWidgetB, 1, 1)
        self.mainLayout.addLayout(self.listLayout)

        self.mainLayout.addWidget(self.buttonResize)
        self.mainLayout.addWidget(self.buttonExit)

        self.setLayout(self.mainLayout)

        # Connect the signals
        self.buttonResize.clicked.connect(self.button_resize_percent_clicked)
        self.buttonStart.clicked.connect(self.button_start_clicked)
        self.buttonDestination.clicked.connect(self.button_destination_clicked)
        self.checkRename.toggled.connect(lambda:self.check_clicked(self.checkRename))
        self.radioButtonA.toggled.connect(self.radio_on_clicked)
        self.radioButtonB.toggled.connect(self.radio_on_clicked)
        # self.sl.textChanged.connect(self.sl_edited)
        self.buttonResize.clicked.connect(lambda: self.check_clicked(self.buttonResize))
        self.buttonExit.clicked.connect(self.button_exit_clicked)

    def button_resize_percent_clicked(self):
        print("Pressed 1!")
        if (self.checkRename.isChecked() == True):
            for p in self.pairs:
                filepath_p = Path(self.folderPathA + "/" + p[0])
                print("yo: " + self.folderPathA + "/" + p[0])
                if filepath_p.is_file():
                    print("File Found?")
                    self.resizeImageByPercent(filepath_p, p[1], self.currFilter) #image name and filter number (to implement)
                else:
                    print("File not found!")
                    print(filepath_p)
                 #   self.displayError(0, p[0])
        else:
            for p in self.pairs:
                filepath_p = Path(self.PATH + p[0])
                if filepath_p.is_file():
                    self.resizeImageByPercent(filepath_p, p[0], self.currFilter)

    def button_exit_clicked(self):
        self.close()

    def check_clicked(self, cb):
        if cb.text() == "Use rename_pairs.csv?":
            if cb.isChecked() == True:
                self.listWidgetB.clear()
                index = 0
                for p in self.pairs:
                    self.listWidgetB.insertItem(index, p[1])
                    index += 1
            else:
                self.listWidgetB.clear()
                index = 0
                for p in self.pairs:
                    self.listWidgetB.insertItem(index, p[0])
                    index += 1

    def radio_on_clicked(self):
        self.radioButton = self.sender()
        self.currFilter = self.radioButton.filter
        if self.radioButton.isChecked():
            print("Filter is: %s" % (self.radioButton.filter))

    # Get the file location start and destination
    def button_start_clicked(self):
        print("Pressed start!")
        self.folderPathA = QFileDialog.getExistingDirectory(self, "Select Input Folder")
        self.lineStart.setText(self.folderPathA)
        csv_path_start = Path(self.folderPathA + "\\rename_pairs.csv")
        print("looking for: " + self.folderPathA + "\\rename_pairs.csv")
        if csv_path_start.is_file():
            print("file found")
            self.pairs.clear()
            self.pairs = self.loadNames(csv_path_start)
            self.populateLists()

        else:
            print("file not found: ")
            print(cvs_path_start)
        for p in self.pairs:
            print(p)

    def button_destination_clicked(self):
        print("Pressed destination!")
        folderPathB = QFileDialog.getExistingDirectory(self, "Select Output Folder")
        self.lineDestination.setText(folderPathB)

    def sl_edited(self):
        print("Saving Location line edited!")

    def populateLists(self):
        index = 0
        for p in self.pairs:
            self.listWidgetA.insertItem(index, p[0])
            self.listWidgetB.insertItem(index, p[1])
            index += 1

    def loadNames(self, fileToParse):
        pairList = []
        with open(fileToParse) as fd:
            rd = csv.reader(fd, delimiter=",", quotechar='"')
            for row in rd:
                pairList.append(row)
        return pairList

    def resizeImageByPercent(self, imageNameA, imageNameB, filter):
        print("called resizeImageByPercent")
        percentage = .01 * float(self.linePercentage.text())
        image = Image.open(imageNameA)
        print(str(image.size[0]) + " " + str(image.size[1]))
        resizedWidth = int((float(image.size[0] * percentage)))
        resizedHeight = int((image.size[1] * percentage))
        image = image.resize((resizedWidth, resizedHeight), filter)
        #image = image.resize((resizedWidth, resizedHeight), "gaston")
        if (self.lineDestination.text() != ""):
            image.save(self.lineDestination.text() + "\\" + imageNameB)
        else:
            print("Choose a location.")
        print(str(resizedWidth) + " " + str(resizedHeight))
        print(percentage)

    #def displayError(self, errorNum, item):
     #   msg = QMessageBox()
      #  msg.setIcon(QMessageBox.Critical)
       # msg.setText("Error")
        #msg.setInformativeText("Hi")
        #msg.setWindowTitle("Error")
#    def resizeListByPercent(self, filter):



if __name__ == "__main__":
    app = QApplication([])
    window = QuickImageResize()
    window.show()
    sys.exit(app.exec())
